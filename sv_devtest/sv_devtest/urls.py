"""sv_devtest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from graphene_django.views import GraphQLView
from rest_framework.documentation import include_docs_urls

from blog.views import BlogListView

urlpatterns = [
    path('', BlogListView.as_view()),
    path('blog/', include('blog.urls')),
    path('comments/', include('comments.urls')),
    path('taxonomies/', include('taxonomies.urls')),

    path("graphql/", GraphQLView.as_view(graphiql=True)),
    path("apidocs/", include_docs_urls(title="DRF API Docs")),
]
