from django import forms

from .models import Taxonomy, BlogTaxonomy


class TaxonomyForm(forms.ModelForm):
    class Meta:
        model = Taxonomy
        fields = ['term']

