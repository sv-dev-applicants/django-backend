from django.test import TransactionTestCase
from faker import Faker

from .models import Blog

fake = Faker()


class BlogTestCase(TransactionTestCase):
    def setUp(self):
        Blog.objects.create(
            title="Test 1",
            body=fake.text()
        )
        Blog.objects.create(
            title="Test 2",
            body=fake.text()
        )

    def test_1_blog_order(self):
        # descending order
        test1 = Blog.objects.first()
        test2 = Blog.objects.last()

        self.assertGreaterEqual(test1.pk, test2.pk)

    def test_2_blog_taxonomy(self):
        test1 = Blog.objects.last()

        tax1 = fake.name()
        tax2 = fake.name()

        test1.add_taxonomy(term=tax1)
        test1.add_taxonomy(term=tax2)

        test1_taxonomies = test1.get_taxonomies()
        """
        assert that the tags were added to the blog
        """
        self.assertEqual(test1_taxonomies.count(), 2)

        test1.add_taxonomy(term=tax2)
        """
        assert that same taxonomy can't be added twice
        """
        self.assertEqual(test1_taxonomies.count(), 2)

    def test_3_blog_comments(self):
        test1 = Blog.objects.last()

        """
        assert that blog has no comments yet
        """
        self.assertEqual(test1.get_comments().count(), 0)

        test1.add_comment(
            author=fake.name(),
            comment=fake.text(),
            email=fake.email()
        )

        """
        assert that comment was added to blog
        """
        self.assertEqual(test1.get_comments().count(), 1)
