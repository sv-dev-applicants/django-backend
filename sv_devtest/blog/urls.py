from django.urls import path, include
from rest_framework import routers

from . import views, api

router = routers.DefaultRouter()
router.register(r'blog', api.BlogViewSet)

urlpatterns = (
    path('api/v1/', include(router.urls)),
    path('list/', views.BlogListView.as_view(), name='blog_list'),
    path('create/', views.BlogCreateView.as_view(), name='blog_create'),
    path('detail/<int:pk>/', views.BlogDetailView.as_view(), name='blog_detail'),
    path('update/<int:pk>/', views.BlogUpdateView.as_view(), name='blog_update'),
)